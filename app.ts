import * as Hapi from 'hapi';

require("dotenv").config()
var vision = require('vision');
var handlebars = require('handlebars');
var inert = require('inert');
var hapiAuthJWT = require('hapi-auth-jwt2');
var JWT = require('jsonwebtoken');

const people = { // user database
    "dasha@gmail.com": {
        password: "111",
        name: 'Daria'
    },
    "alex@gmail.com": {
        password: "222",
        name: 'Alex'
    }
};

const init = async () => {

    const server = new Hapi.Server({
        port: 3000,
        host: 'localhost'
    });

    await server.register(vision);
    await server.register(inert);
    await server.register(hapiAuthJWT);

    //setup auth mechanism
    const validate = async function (decoded, request) {
        let userExists = await User.exists(decoded.email)
            .then((user)=>{return true})
            .catch((err)=>{return false});
        
        if (userExists) { //check if user exists
            return { isValid: true, credentials: decoded, artifacts: request.auth.token};
        }else {
            return { isValid: false };
        }
    };

    server.auth.strategy('jwt', 'jwt',
        {
            key: process.env.JWT_SECRET,
            redirectTo:'/login',
            validate: validate,
            verifyOptions: { algorithms: [ 'HS256' ] }
        });
    server.auth.default('jwt');

    //setup session cache
    let tokenCookies = await server.state('token', { // for storing jwt
        ttl: 1000*60*60*24,
        isSecure: false,
        isHttpOnly: false,
        encoding: 'none', //need no encoding
        clearInvalid: true
    });

    const sendFile = (filepath) => { // function for send static resource
        return (request, h) => {
            return h.file(filepath);
        }
    }

    server.route([
        {
            method: 'GET',
            path: '/style.css',
            options:{auth:false},
            handler: sendFile('./views/styles/style.css')
        },
        {
            method: 'GET',
            path: '/bootstrap.min.js',
            options:{auth:false},
            handler: sendFile('./node_modules/bootstrap/dist/js/bootstrap.min.js')
        },
        {
            method: 'GET',
            path: '/icons/MaterialIcons-Regular.eot',
            options:{auth:false},
            handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.eot')
        },
        {
            method: 'GET',
            path: '/icons/MaterialIcons-Regular.woff2',
            options:{auth:false},
            handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.woff2')
        },
        {
            method: 'GET',
            path: '/icons/MaterialIcons-Regular.woff',
            options:{auth:false},
            handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.woff')
        },
        {
            method: 'GET',
            path: '/icons/MaterialIcons-Regular.ttf',
            options:{auth:false},
            handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.ttf')
        }
    ]); //static content

    server.views({ // hendlbars configuration
        engines: {html: handlebars},
        relativeTo: __dirname,
        path: 'views',
        layout: true,
        layoutPath: 'views'
    });

    let {User, ToDo} = await initDB();



    server.route({
        method: 'GET',
        path: '/login',
        options:{auth:false},
        handler: (request, h) => {
            return h.view('login');
        }
    }); //GET login page
    server.route({
        method: 'POST',
        path: '/login',
        options:{auth:false},
        handler: async (request, h) => {
            //get email and password from request parameters
            var email = request.payload['email'];
            var password = request.payload['password'];

            var response;

            await User.authenticate(email,password)
                .then((authenticatedUser)=>{
                    var credentials = {
                        email:authenticatedUser.email,
                        password:authenticatedUser.password
                    }

                    var token = JWT.sign(credentials, "JWTSecretKey"); // sign user credentials

                    response = h.response({token:token});
                    response.code(200);
                    response.state("token",token);
                })
                .catch((error)=>{
                    response = h.response({error: "Authentication error. Check fields and try again..."});
                    response.code(403);
                })

            return response;
        }
    }); //POST login
    server.route({
        method: 'GET',
        path: '/registration',
        options:{
            auth:false,
            },
        handler: (request, h) => {
            return h.view('registration');
        }
    }); //GET registration
    server.route({
        method: 'POST',
        path: '/registration',
        options:{
            auth:false,
        },
        handler: async (request, h) => {
            //get email and password from request parameters
            var email = request.payload['email'];
            var name = request.payload['name'];
            var password = request.payload['password'];

            var response;

            await User.register(email,name,password)
                .then((registeredUser)=>{
                    var credentials = {
                        email:registeredUser.email,
                        password:registeredUser.password
                    }

                    var token = JWT.sign(credentials, "JWTSecretKey"); // sign user credentials

                    response = h.response({token:token});
                    response.code(200);
                    response.state("token",token);
                })
                .catch((error)=>{
                    response = h.response({error: "Registration error. " + error.message});
                    response.code(403);
                })

            return response;
        }
    }); //POST registration
    server.route({
        method: ['GET','POST'],
        path: '/logout',
        options:{auth:"jwt"},
        handler: (request, h) => {
            return h.response({})
                .unstate("token")
                .redirect("/login")
        }
    }); //logout
    server.route({
        method: 'GET',
        path: '/list',
        options:{auth:"jwt"},
        handler: async (request, h) => {
            var user = await User.exists(request.auth.credentials['email'])
            var foundTodos = await ToDo.find({userId:user._id})
                .then((found)=>{ return found})
                .catch((err)=>{return []})

            return h.view('list',{
                username:user.name,
                todos:JSON.stringify(foundTodos),
            });
        },
    }); //GET list

    server.route({
        method: 'GET',
        path: '/todo',
        options:{auth:"jwt"},
        handler: async (request, h) => {
            var user = await User.exists(request.auth.credentials['email'])
            var foundTodos = await ToDo.find({userId:user._id}).sort('-date').exec()
                .then((found)=>{ return found})
                .catch((err)=>{return []})

            return h.response(foundTodos);
        },
    }); //GET todos
    server.route({
        method: 'POST',
        path: '/todo/update/{id}',
        options:{auth:"jwt"},
        handler: async (request, h) => {
            var todoId = request.params.id
            var user = await User.exists(request.auth.credentials['email'])

            var response;

            await ToDo.findOneAndUpdate(
                {userId:user._id,_id:todoId},
                {text:request.payload['text'],date: new Date(request.payload['date']),status: request.payload['status'] })
                .then((foundTodo)=>{
                    response = h.response(foundTodo).code(200);
                })
                .catch((e)=>{
                    response = h.response({error:e}).code(500);
                })


            return response;
        },
    }); //update todos
    server.route({
        method: 'GET',
        path: '/todo/remove/{id}',
        options:{auth:"jwt"},
        handler: async (request, h) => {
            var todoId = request.params.id
            var user = await User.exists(request.auth.credentials['email'])

            var response;

            await ToDo.findOneAndDelete({userId:user._id,_id:todoId})
                .then((foundTodo)=>{
                    response = h.response({}).code(200);
                })
                .catch((e)=>{
                    response = h.response({error:e}).code(500);
                })


            return response;
        },
    });
    server.route({
        method: 'POST',
        path: '/todo/create',
        options:{auth:"jwt"},
        handler: async (request, h) => {
            var user = await User.exists(request.auth.credentials['email'])
            let todo = new ToDo({userId:user._id,text:request.payload['text'],date: request.payload['date'],status:request.payload['status']})

            var response;

            await todo.save()
                .then((savedTodo)=>{
                    response = h.response(savedTodo).code(200);
                })
                .catch((e)=>{
                    response = h.response({error:e}).code(500);
                })


            return response;
        },
    })

    await server.start();
    return server;
};

init()
    .then(server => {
        console.log('Server running at:', server.info.uri);
    })
    .catch(error => {
        console.log(error);
    });

const initDB = ()=>{
    return new Promise(function(resolve,reject){

        //init mongo db
        var mongoose = require('mongoose');
        mongoose.connect(process.env.MONGODB_URI);

        var db = mongoose.connection;
        db.on('error', function(error){
            reject(error);
        });

        db.once('open', function() {

            var userSchema = new mongoose.Schema({
                email: String,
                name: String,
                password: String
            });

            userSchema.statics.register = (email,name,password)=>{
                return new Promise(function(resolve,reject){
                    User.count({email:email},function(err,count){
                        if(err)return reject(err);
                        if(count>0)return reject(new Error("Email occupied"))
                        var userToRegister = new User({email:email,name:name,password:password})
                        userToRegister.save()
                            .then(resolve,reject);
                    })
                })
            }

            userSchema.statics.authenticate = (email,password)=>{
                return new Promise(function(resolve,reject){
                    User.findOne({email:email,password:password},function(err,user){
                        if(err)reject(err);
                        resolve(user);
                    })
                })
            }

            userSchema.statics.exists = (email) =>{
                return new Promise(function(resolve,reject){
                    User.findOne({email:email},function(err,foundUser){
                        if (err) return reject(err);
                        if (!foundUser) return reject(new Error("User not found"))
                        resolve (foundUser)
                    })
                })
            }

            var User = mongoose.model('User', userSchema);

            var todoSchema = new mongoose.Schema({
                userId: String,
                text: String,
                date: Date,
                status: String
            })

            var ToDo = mongoose.model("ToDo", todoSchema);

            resolve({User:User,ToDo:ToDo})
        });

    })
}




