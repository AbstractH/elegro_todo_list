"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
exports.__esModule = true;
var Hapi = require("hapi");
require("dotenv").config();
var vision = require('vision');
var handlebars = require('handlebars');
var inert = require('inert');
var hapiAuthJWT = require('hapi-auth-jwt2');
var JWT = require('jsonwebtoken');
var people = {
    "dasha@gmail.com": {
        password: "111",
        name: 'Daria'
    },
    "alex@gmail.com": {
        password: "222",
        name: 'Alex'
    }
};
var init = function () { return __awaiter(_this, void 0, void 0, function () {
    var _this = this;
    var server, validate, tokenCookies, sendFile, _a, User, ToDo;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                server = new Hapi.Server({
                    port: 3000,
                    host: 'localhost'
                });
                return [4 /*yield*/, server.register(vision)];
            case 1:
                _b.sent();
                return [4 /*yield*/, server.register(inert)];
            case 2:
                _b.sent();
                return [4 /*yield*/, server.register(hapiAuthJWT)];
            case 3:
                _b.sent();
                validate = function (decoded, request) {
                    return __awaiter(this, void 0, void 0, function () {
                        var userExists;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, User.exists(decoded.email)
                                        .then(function (user) { return true; })["catch"](function (err) { return false; })];
                                case 1:
                                    userExists = _a.sent();
                                    if (userExists) { //check if user exists
                                        return [2 /*return*/, { isValid: true, credentials: decoded, artifacts: request.auth.token }];
                                    }
                                    else {
                                        return [2 /*return*/, { isValid: false }];
                                    }
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                server.auth.strategy('jwt', 'jwt', {
                    key: process.env.JWT_SECRET,
                    redirectTo: '/login',
                    validate: validate,
                    verifyOptions: { algorithms: ['HS256'] }
                });
                server.auth["default"]('jwt');
                return [4 /*yield*/, server.state('token', {
                        ttl: 1000 * 60 * 60 * 24,
                        isSecure: false,
                        isHttpOnly: false,
                        encoding: 'none',
                        clearInvalid: true
                    })];
            case 4:
                tokenCookies = _b.sent();
                sendFile = function (filepath) {
                    return function (request, h) {
                        return h.file(filepath);
                    };
                };
                server.route([
                    {
                        method: 'GET',
                        path: '/style.css',
                        options: { auth: false },
                        handler: sendFile('./views/styles/style.css')
                    },
                    {
                        method: 'GET',
                        path: '/bootstrap.min.js',
                        options: { auth: false },
                        handler: sendFile('./node_modules/bootstrap/dist/js/bootstrap.min.js')
                    },
                    {
                        method: 'GET',
                        path: '/icons/MaterialIcons-Regular.eot',
                        options: { auth: false },
                        handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.eot')
                    },
                    {
                        method: 'GET',
                        path: '/icons/MaterialIcons-Regular.woff2',
                        options: { auth: false },
                        handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.woff2')
                    },
                    {
                        method: 'GET',
                        path: '/icons/MaterialIcons-Regular.woff',
                        options: { auth: false },
                        handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.woff')
                    },
                    {
                        method: 'GET',
                        path: '/icons/MaterialIcons-Regular.ttf',
                        options: { auth: false },
                        handler: sendFile('./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.ttf')
                    }
                ]); //static content
                server.views({
                    engines: { html: handlebars },
                    relativeTo: __dirname,
                    path: 'views',
                    layout: true,
                    layoutPath: 'views'
                });
                return [4 /*yield*/, initDB()];
            case 5:
                _a = _b.sent(), User = _a.User, ToDo = _a.ToDo;
                server.route({
                    method: 'GET',
                    path: '/login',
                    options: { auth: false },
                    handler: function (request, h) {
                        return h.view('login');
                    }
                }); //GET login page
                server.route({
                    method: 'POST',
                    path: '/login',
                    options: { auth: false },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var email, password, response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    email = request.payload['email'];
                                    password = request.payload['password'];
                                    return [4 /*yield*/, User.authenticate(email, password)
                                            .then(function (authenticatedUser) {
                                            var credentials = {
                                                email: authenticatedUser.email,
                                                password: authenticatedUser.password
                                            };
                                            var token = JWT.sign(credentials, "JWTSecretKey"); // sign user credentials
                                            response = h.response({ token: token });
                                            response.code(200);
                                            response.state("token", token);
                                        })["catch"](function (error) {
                                            response = h.response({ error: "Authentication error. Check fields and try again..." });
                                            response.code(403);
                                        })];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/, response];
                            }
                        });
                    }); }
                }); //POST login
                server.route({
                    method: 'GET',
                    path: '/registration',
                    options: {
                        auth: false
                    },
                    handler: function (request, h) {
                        return h.view('registration');
                    }
                }); //GET registration
                server.route({
                    method: 'POST',
                    path: '/registration',
                    options: {
                        auth: false
                    },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var email, name, password, response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    email = request.payload['email'];
                                    name = request.payload['name'];
                                    password = request.payload['password'];
                                    return [4 /*yield*/, User.register(email, name, password)
                                            .then(function (registeredUser) {
                                            var credentials = {
                                                email: registeredUser.email,
                                                password: registeredUser.password
                                            };
                                            var token = JWT.sign(credentials, "JWTSecretKey"); // sign user credentials
                                            response = h.response({ token: token });
                                            response.code(200);
                                            response.state("token", token);
                                        })["catch"](function (error) {
                                            response = h.response({ error: "Registration error. " + error.message });
                                            response.code(403);
                                        })];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/, response];
                            }
                        });
                    }); }
                }); //POST registration
                server.route({
                    method: ['GET', 'POST'],
                    path: '/logout',
                    options: { auth: "jwt" },
                    handler: function (request, h) {
                        return h.response({})
                            .unstate("token")
                            .redirect("/login");
                    }
                }); //logout
                server.route({
                    method: 'GET',
                    path: '/list',
                    options: { auth: "jwt" },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var user, foundTodos;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, User.exists(request.auth.credentials['email'])];
                                case 1:
                                    user = _a.sent();
                                    return [4 /*yield*/, ToDo.find({ userId: user._id })
                                            .then(function (found) { return found; })["catch"](function (err) { return []; })];
                                case 2:
                                    foundTodos = _a.sent();
                                    return [2 /*return*/, h.view('list', {
                                            username: user.name,
                                            todos: JSON.stringify(foundTodos)
                                        })];
                            }
                        });
                    }); }
                }); //GET list
                server.route({
                    method: 'GET',
                    path: '/todo',
                    options: { auth: "jwt" },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var user, foundTodos;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, User.exists(request.auth.credentials['email'])];
                                case 1:
                                    user = _a.sent();
                                    return [4 /*yield*/, ToDo.find({ userId: user._id }).sort('-date').exec()
                                            .then(function (found) { return found; })["catch"](function (err) { return []; })];
                                case 2:
                                    foundTodos = _a.sent();
                                    return [2 /*return*/, h.response(foundTodos)];
                            }
                        });
                    }); }
                }); //GET todos
                server.route({
                    method: 'POST',
                    path: '/todo/update/{id}',
                    options: { auth: "jwt" },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var todoId, user, response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    todoId = request.params.id;
                                    return [4 /*yield*/, User.exists(request.auth.credentials['email'])];
                                case 1:
                                    user = _a.sent();
                                    return [4 /*yield*/, ToDo.findOneAndUpdate({ userId: user._id, _id: todoId }, { text: request.payload['text'], date: new Date(request.payload['date']), status: request.payload['status'] })
                                            .then(function (foundTodo) {
                                            response = h.response(foundTodo).code(200);
                                        })["catch"](function (e) {
                                            response = h.response({ error: e }).code(500);
                                        })];
                                case 2:
                                    _a.sent();
                                    return [2 /*return*/, response];
                            }
                        });
                    }); }
                }); //update todos
                server.route({
                    method: 'GET',
                    path: '/todo/remove/{id}',
                    options: { auth: "jwt" },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var todoId, user, response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    todoId = request.params.id;
                                    return [4 /*yield*/, User.exists(request.auth.credentials['email'])];
                                case 1:
                                    user = _a.sent();
                                    return [4 /*yield*/, ToDo.findOneAndDelete({ userId: user._id, _id: todoId })
                                            .then(function (foundTodo) {
                                            response = h.response({}).code(200);
                                        })["catch"](function (e) {
                                            response = h.response({ error: e }).code(500);
                                        })];
                                case 2:
                                    _a.sent();
                                    return [2 /*return*/, response];
                            }
                        });
                    }); }
                });
                server.route({
                    method: 'POST',
                    path: '/todo/create',
                    options: { auth: "jwt" },
                    handler: function (request, h) { return __awaiter(_this, void 0, void 0, function () {
                        var user, todo, response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, User.exists(request.auth.credentials['email'])];
                                case 1:
                                    user = _a.sent();
                                    todo = new ToDo({ userId: user._id, text: request.payload['text'], date: request.payload['date'], status: request.payload['status'] });
                                    return [4 /*yield*/, todo.save()
                                            .then(function (savedTodo) {
                                            response = h.response(savedTodo).code(200);
                                        })["catch"](function (e) {
                                            response = h.response({ error: e }).code(500);
                                        })];
                                case 2:
                                    _a.sent();
                                    return [2 /*return*/, response];
                            }
                        });
                    }); }
                });
                return [4 /*yield*/, server.start()];
            case 6:
                _b.sent();
                return [2 /*return*/, server];
        }
    });
}); };
init()
    .then(function (server) {
    console.log('Server running at:', server.info.uri);
})["catch"](function (error) {
    console.log(error);
});
var initDB = function () {
    return new Promise(function (resolve, reject) {
        //init mongo db
        var mongoose = require('mongoose');
        mongoose.connect(process.env.MONGODB_URI);
        var db = mongoose.connection;
        db.on('error', function (error) {
            reject(error);
        });
        db.once('open', function () {
            var userSchema = new mongoose.Schema({
                email: String,
                name: String,
                password: String
            });
            userSchema.statics.register = function (email, name, password) {
                return new Promise(function (resolve, reject) {
                    User.count({ email: email }, function (err, count) {
                        if (err)
                            return reject(err);
                        if (count > 0)
                            return reject(new Error("Email occupied"));
                        var userToRegister = new User({ email: email, name: name, password: password });
                        userToRegister.save()
                            .then(resolve, reject);
                    });
                });
            };
            userSchema.statics.authenticate = function (email, password) {
                return new Promise(function (resolve, reject) {
                    User.findOne({ email: email, password: password }, function (err, user) {
                        if (err)
                            reject(err);
                        resolve(user);
                    });
                });
            };
            userSchema.statics.exists = function (email) {
                return new Promise(function (resolve, reject) {
                    User.findOne({ email: email }, function (err, foundUser) {
                        if (err)
                            return reject(err);
                        if (!foundUser)
                            return reject(new Error("User not found"));
                        resolve(foundUser);
                    });
                });
            };
            var User = mongoose.model('User', userSchema);
            var todoSchema = new mongoose.Schema({
                userId: String,
                text: String,
                date: Date,
                status: String
            });
            var ToDo = mongoose.model("ToDo", todoSchema);
            resolve({ User: User, ToDo: ToDo });
        });
    });
};
